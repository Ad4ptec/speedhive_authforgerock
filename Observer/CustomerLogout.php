<?php
namespace Speedhive\AuthForgerock\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer;


class CustomerLogout implements ObserverInterface {
	
	protected $auth;
	
	public function __construct(
		\Speedhive\AuthForgerock\Helper\Api $auth
	) {
		$this->auth = $auth;
	}
	
	/**
	 * This is the method that fires when the event runs. 
	 * 
	 * @param Observer $observer
	 */
	public function execute(Observer $observer) {
		$this->auth->init()->unsetCookie();
	}
}