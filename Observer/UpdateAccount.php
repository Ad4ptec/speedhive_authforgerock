<?php
namespace Speedhive\AuthForgerock\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer;


class UpdateAccount implements ObserverInterface {
	
	protected $auth;
	protected $request;
	
	public function __construct(
		\Speedhive\AuthForgerock\Helper\Api $auth,
		\Magento\Framework\App\Request\Http $request
	) {
		$this->auth = $auth;
		$this->request = $request;
	}
	
	/**
	 * This is the method that fires when the event runs. 
	 * 
	 * @param Observer $observer
	 */
	public function execute(Observer $observer) {
		if ($this->request->getActionName() == 'createpost') {
			return;
		}
		
		$customer = $observer->getEvent()->getCustomerDataObject();
		$this->auth->init();
		
		if (!$this->auth->getUserByEmail($customer->getEmail())) {
			if (!$this->auth->cookie || !$this->auth->getUserId()) {
				return;
			}
			
			if (!$this->auth->getUser($this->auth->forgerock->userid)) {
				return;
			}
		}
		
		if (!isset($this->auth->forgerock->userid)) {
			return;
		}
		
		if ($this->request->getPost('email')) {
			$this->auth->forgerock->mail = $this->request->getPost('email');
			$this->auth->saveAll();
		}
		
		if ($this->request->getPost('password') && !empty($this->request->getPost('password'))) {
			$this->auth->forgerock->userPassword = $this->request->getPost('password');
		}
		
		$this->auth->forgerock->givenName = $this->request->getPost('firstname');
		$this->auth->forgerock->surname = $this->request->getPost('lastname');
		$this->auth->forgerock->commonname = "{$this->auth->forgerock->givenName} {$this->auth->forgerock->surname}";
		
		$this->auth->save();
		
		return;
	}
}