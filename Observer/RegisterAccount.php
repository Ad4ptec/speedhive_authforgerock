<?php
namespace Speedhive\AuthForgerock\Observer;

use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;
use Magento\Customer\Api\Data\CustomerInterface;


class RegisterAccount implements ObserverInterface {
	
	protected $auth;
	protected $request;
	protected $_storeManager;
	
	public function __construct(
		\Speedhive\AuthForgerock\Helper\Api $auth,
		\Magento\Framework\App\Request\Http $request,
		\Magento\Store\Model\StoreManagerInterface $storeManager
	) {
		$this->auth = $auth;
		$this->request = $request;
		$this->_storeManager = $storeManager;
	}
	
	/**
	 * This is the method that fires when the event runs. 
	 * @param Observer $observer
	 */
	public function execute(EventObserver $observer) {
		$user = $this->request->getPost();
		
		$data = array(
			'mail' => $user['email'],
			'userPassword' => $user['password'],
			'givenName' => $user['firstname'],
			'surname' => $user['lastname'],
			'commonname' => "{$user['firstname']} {$user['lastname']}",
			'inetUserStatus' => 'ACTIVE',
		);
		
		$this->auth->init();
		
		$this->auth->getUserByEmail($user['email']);
		
		if (isset($this->auth->forgerock->userid) || $this->auth->setUser($data)) {
			$base_url = $this->_storeManager->getStore()->getBaseUrl();
			header("Location: {$this->auth->settings->login_url}?goto={$base_url}customer/account/");
			exit;
		}
	}
}