<?php
namespace Speedhive\AuthForgerock\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer;


class GlobalEvent implements ObserverInterface {
	
	protected $auth;
	protected $api;
	protected $registry;
	protected $urlHelper;
	
	public function __construct(
		\Speedhive\AuthForgerock\Helper\Api $auth,
		\Speedhive\MyLapsIntegration\Helper\Api $api,
		\Magento\Framework\Registry $registry,
		\Magento\Framework\Url $urlHelper
	) {
		$this->auth = $auth;
		$this->api = $api;
		$this->registry = $registry;
		$this->urlHelper = $urlHelper;
	}
	
	/**
	 * This is the method that fires when the event runs. 
	 * 
	 * @param Observer $observer
	 */
	public function execute(Observer $observer) {
		// if (isset($_GET['test'])) {
			// print '<pre>';
			
			// $this->api->getAccessToken();
			// var_export($this->api->GetRandomTransponderNumbers(1), true);
			// exit;
			// $random_transponder = isset($_GET['number'])? $_GET['number']: mt_rand(1000000, 9999999);
			// $this->api->TransponderNumber = $random_transponder;
			// $this->api->TransponderNumber = $this->api->CheckIfTransponderNumberIsFree()->TransponderNumber;
			
			// $free_transponder = $this->api->TemporaryLockTransponderNumber();
			// $this->api->TVC = $free_transponder->TVC;
			// $this->api->TransponderID = $free_transponder->TransponderID;
			
			// $Instance = $this->api->SetInstanceIdsForTransponderNumber(1);
			
			// $this->api->InstanceID = $Instance->InstanceID;
			// $this->api->SetStatusToTransponderInstances('Locked');
			// $this->api->SetStatusToTransponders('Locked');
			// $this->api->GetInstanceIdsByTransponderNumber();
			
			// exit;
		// }
		
		
		$this->auth->init();
		
		if (!$this->auth->cookie) {
			return;
		}
		
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		
		if ($objectManager->get('Magento\Customer\Model\Session')->getCustomerId()) {
			return;
		}
		
		if (!$this->auth->getUserId()) {
			return;
		}
		
		if (!$this->auth->getUser($this->auth->forgerock->userid)) {
			return;
		}
		
		$customer_model = $objectManager->create('Magento\Customer\Model\Customer');
		$customer_collection = $customer_model->getCollection()->addFieldToFilter('email', $this->auth->forgerock->mail);
		$customer = $customer_collection->load();
		$customer_data = $customer->getData();
		
		if ($customer_data) {
			$customerSession = $objectManager->create('Magento\Customer\Model\Session');
			$customerSession->loginById($customer_data[0]['entity_id']);
		} else {
			$websiteId  = $objectManager->create('Magento\Store\Model\StoreManagerInterface')->getWebsite()->getWebsiteId();
			
			$customer = $objectManager->create('Magento\Customer\Model\CustomerFactory')->create();
			$customer->setWebsiteId($websiteId)
				->setEmail($this->auth->forgerock->mail)
				->setFirstname($this->auth->forgerock->givenName)
				->setLastname($this->auth->forgerock->surname)
				->setDob(date('Y-m-d', $this->auth->forgerock->dateOfBirth))
				->setPassword($this->auth->forgerock->userPassword)
				->save();
			
			$address = $objectManager->create('Magento\Customer\Model\Address');
			$address->setData([
					'firstname' => $this->auth->forgerock->givenName,
					'lastname' => $this->auth->forgerock->surname,
					'street' => array (
						'0' => $this->auth->forgerock->postalAddress,
					),
					'city' => $this->auth->forgerock->localityname,
					'region_id' => '',
					'region' => $this->auth->forgerock->stateOrProvincename,
					'postcode' => $this->auth->forgerock->postalCode,
					'country_id' => $this->auth->forgerock->countryname,
					'telephone' => $this->auth->forgerock->telephoneNumber,
				])
				->setCustomerId($customer->getId())
				->setIsDefaultBilling('1')
				->setIsDefaultShipping('1')
				->setSaveInAddressBook('1')
				->save();
		}
	}
}