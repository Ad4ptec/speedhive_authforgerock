<?php
namespace Speedhive\AuthForgerock\Helper;

use \Magento\Framework\App\Helper\AbstractHelper;

class Api extends AbstractHelper
{
	protected $scopeConfig;
	protected $objectManager;
	protected $cookieManager;
	
	public $settings;
	public $forgerock = array();
	public $cookie;
	
	public function __construct(
		\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
	) {
		$this->scopeConfig = $scopeConfig;
		$this->forgerock = (object) $this->forgerock;
	}
	
	public function init()
	{
		$this->getSettings();
		$this->getCookie();
		
		return $this;
	}
	
	public function getCookie() {
		if (!$this->cookie) {
			$this->objectManager = \Magento\Framework\App\ObjectManager::getInstance();
			$this->cookieManager = $this->objectManager->get('Magento\Framework\Stdlib\CookieManagerInterface');
			$this->cookie = $this->cookieManager->getCookie('iPlanetDirectoryPro')?: false;
		}
		
		return $this->cookie;
	}
	
	public function unsetCookie() {
		$this->cookie = $this->cookieManager->deleteCookie('iPlanetDirectoryPro')?: false;
		
		if ($this->cookie) {
			$this->cookieManager->setPublicCookie('iPlanetDirectoryPro', NULL);
			$this->cookie = NULL;
		}
		
		return $this->cookie;
	}
	
	public function getSettings() {
		if (!$this->settings) {
			$this->settings = (object) $this->scopeConfig->getValue(
				'authforgerock_settings/general',
				\Magento\Store\Model\ScopeInterface::SCOPE_STORE
			);
		}
		
		return $this->settings;
	}
	
	/*
	 * Get user id by cookie
	*/
	public function getUserId()
	{
		if (!isset($this->forgerock->userid)) {
			$this->forgerock->userid = $this->getResponse("account.json/closed/get/{$this->cookie}");
		}
		
		return $this->forgerock->userid?: false;
	}
	
	/*
	 * Create new user
	*/
	public function setUser($data)
	{
		$this->forgerock = json_decode($this->getResponse("person.json", $data));
		return $this->forgerock?: false;
	}
	
	/*
	 * Update all userdata
	*/
	public function saveAll()
	{
		$this->forgerock = json_decode($this->getResponse("person.json/updateall?id={$this->forgerock->userid}", $this->forgerock));
		return $this->forgerock?: false;
	}
	
	/*
	 * Update userdata
	*/
	public function save()
	{
		$this->forgerock = json_decode($this->getResponse("person.json/{$this->forgerock->userid}", $this->forgerock));
		return $this->forgerock?: false;
	}
	
	/*
	 * Get user by user id
	*/
	public function getUser($id)
	{
		$this->forgerock = json_decode($this->getResponse("person.json/{$id}"));
		return $this->forgerock?: false;
	}
	
	/*
	 * Get user by user email
	*/
	public function getUserByEmail($email)
	{
		$this->forgerock = json_decode($this->getResponse("person.json/mail/{$email}"));
		return $this->forgerock?: false;
	}
	
	/*
	 * Get response
	*/
	public function getResponse($method, $data = array())
	{
		if(!isset($this->settings->api_url))
			return false;
		
		$url = "{$this->settings->api_url}/{$method}";
		
		$ch = curl_init($url);
		
		if ($data) {
			$body = json_encode($data, JSON_PRETTY_PRINT);
			
			curl_setopt($ch, CURLOPT_HTTPHEADER, array(
				'Content-Type: application/json',
				'Content-Length: ' . strlen($body)
			));
			
			curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
			curl_setopt($ch, CURLOPT_POST, true);
		}
		
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HEADER, false);
		
		$response = curl_exec($ch);
		curl_close($ch);
		
		return $response;
	}
}